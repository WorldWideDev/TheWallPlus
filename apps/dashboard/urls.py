from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'create/post$', views.create_post),
    url(r'create/comment/(?P<post_id>\d+)$', views.create_comment),
]