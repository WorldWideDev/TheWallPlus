# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect
from .forms import CreatePostForm, CreateCommentForm
from .models import Post, Comment
from .view_helpers import get_template_data, initialize_context

TEMPLATE_URL = 'dashboard/index.html'

MAIN_CONTEXT = initialize_context()

# Create your views here.
@login_required
def index(req):
    #TODO(dev): we are double querying here, OPTIMZE THIS
    MAIN_CONTEXT = initialize_context()
    
    return render(req, TEMPLATE_URL, MAIN_CONTEXT)

def create_post(req):
    submitted = CreatePostForm(req.POST)
    if submitted.is_valid():
        author = req.user
        new_post = submitted.save(commit=False)
        new_post.author = author
        new_post.save()        
        return redirect('/dashboard')

    MAIN_CONTEXT['forms']['post'] = submitted
    MAIN_CONTEXT["data"] = get_template_data()
    return render(req, TEMPLATE_URL, MAIN_CONTEXT)

def create_comment(req, post_id):
    submitted_comment = CreateCommentForm(req.POST)
    if submitted_comment.is_valid():
        user = req.user
        post = Post.objects.get(id=post_id)
        new_comment = submitted_comment.save(commit=False)
        new_comment.author = user
        new_comment.rel_post = post
        new_comment.save()
        return redirect('/dashboard')

    MAIN_CONTEXT['forms']['bound_comment_form'] = submitted_comment
    MAIN_CONTEXT['bound_post'] = int(post_id)
    MAIN_CONTEXT["data"] = get_template_data()
    return render(req, TEMPLATE_URL, MAIN_CONTEXT)