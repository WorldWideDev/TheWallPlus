from django import template
from ..models import Post, Comment
from django.utils import timezone, timesince
register = template.Library()

@register.simple_tag
def time_since_posted(post_id):
    post = Post.objects.get(id=post_id)
    posted = post.updated_at
    now = timezone.now()
    return timesince.timesince(posted)