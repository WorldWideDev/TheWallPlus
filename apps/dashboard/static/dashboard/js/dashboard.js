var textareas = document.getElementsByTagName("textarea");

$('.edit-post-btn').click(function(e){
    e.preventDefault()
    var canEdit = $(this).attr('can-edit');
    var sibs = ($(this).siblings())
    var postContent = $(this).parent().parent().children()[1]
    var label = $(sibs[1]).children()[0];
    var textArea = $(sibs[1]).children()[1]
    if(canEdit == 'false'){
        var pText = $(postContent).text()
        $(postContent).attr('alt-text', pText)
        $(label).text("Edit this post:");
        $(this).text("Cancel Edit")
        $(textArea).text(pText)

    } else {
        var altText = $(postContent).attr('alt-text');
        $(label).text("Comment on this post:");
        $(this).text("Switch to Edit")
        $(textArea).html('')
        $(postContent).text(altText);
    }
    canEdit = (canEdit == 'true') ? 'false' : 'true';
    $(this).attr('can-edit', canEdit)

    // optomizing the event attachments
    var idxOfTextarea = $(textareas).index(textArea)
    attachTextareaEvents(idxOfTextarea)
})

function attachTextareaEvents(selected){
    for(var i = 0; i <= selected; i++){
        (function(textarea){
            console.log(textareaPermissions(textarea))
            if(textareaPermissions(textarea).hasThird){
                textarea.addEventListener('input', function(e){
                    if(textareaPermissions(textarea).canEdit){
                        var pc = getPostContent(textarea)
                        $(pc).text(e.target.value);
                    }
                })
            }
        })(textareas[i])
    }
}

function getPostContent(ta){
    return $($(ta).parent().parent().siblings()[1])
}
function textareaPermissions(ta){
    var hasThirdButton = $(ta).parent().siblings().length > 2;
    var canEdit = $($(ta).parent().siblings()[2]).attr('can-edit') == 'true';
    return {
        hasThird: hasThirdButton,
        canEdit: canEdit
    }
}