from django.forms import ModelForm
from .models import Post, Comment
from .fields import PostField, CommentField

class CreatePostForm(ModelForm):
    content = PostField()
    class Meta:
        model = Post
        fields = ['content'] 

class CreateCommentForm(ModelForm):
    content = CommentField()
    class Meta:
        model = Comment
        fields = ['content']