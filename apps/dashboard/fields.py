from django import forms
from django.core import validators

class PostField(forms.CharField):
    MIN_LENGTH = 5
    PostMinLengthValidator = validators.MinLengthValidator(
        MIN_LENGTH,
        "Please write a little more!"
    )
    def __init__(self, *args, **kwargs):
        super(PostField, self).__init__(*args, **kwargs)
        self.validators.append(PostField.PostMinLengthValidator)
        self.widget = forms.Textarea(attrs = {
        'cols': '50',
        'rows' : '7'
        })

class CommentField(PostField):
    def __init__(self, *args, **kwargs):
        super(CommentField, self).__init__(*args, **kwargs)
        self.widget = forms.Textarea(attrs = {
        'cols': '40',
        'rows' : '5'
        })
        self.label = "Comment on this post"
