from .models import Post, Comment
from .forms import CreateCommentForm, CreatePostForm

def get_template_data():
    return {
        "posts": Post.objects.all().order_by('-updated_at'),
        "comments": Comment.objects.all()
    }

def initialize_context():
    print 'initializing context'
    return {
        "data": get_template_data(),
        "forms": {
            "post": CreatePostForm(),
            "comment": CreateCommentForm()
        }
    }