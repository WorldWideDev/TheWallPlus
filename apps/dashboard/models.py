# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ..loginreg.models import WallUser
from django.db import models
from datetime import datetime, timedelta
from django.utils import timezone
from django.utils.timesince import timesince
from django.utils.translation import ugettext_lazy as _
from shared_util.utc import utc

# 15 minutes (in seconds)
EDIT_TIME_ALLOWED = 60 * 60

# Create your models here.
class Post(models.Model):
    content = models.TextField()
    author = models.ForeignKey(WallUser, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
    

    def time_since_updated(self):
        return datetime.now(utc) - self.updated_at

    def time_since_updated_format(self):
        hours, rem = divmod(self.time_since_updated().total_seconds(), 3600)
        minutes, seconds = divmod(rem, 60)
        return '%s:%s:%s' % (int(hours), int(minutes), int(seconds))

    def can_edit(self):
        return self.time_since_updated().total_seconds() < EDIT_TIME_ALLOWED

    def __unicode__(self):
        return "Post Object {} created by {}".format(self.id, self.author.first_name)
   
class Comment(models.Model):
    content = models.TextField()
    author = models.ForeignKey(WallUser, null=True, on_delete=models.SET_NULL)
    rel_post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')