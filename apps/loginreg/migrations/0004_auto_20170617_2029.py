# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-18 03:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loginreg', '0003_auto_20170617_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='walluser',
            name='birthday',
            field=models.DateTimeField(blank=True, verbose_name='Date of Birth'),
        ),
        migrations.AlterField(
            model_name='walluser',
            name='first_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='First Name'),
        ),
        migrations.AlterField(
            model_name='walluser',
            name='last_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='Last Name'),
        ),
    ]
